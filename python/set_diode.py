from sense_hat import SenseHat
import sys
sense = SenseHat()
error = 0

if len(sys.argv) == 6:
	if int(sys.argv[1])<0 or int(sys.argv[1])>7:
		error = 1
		print("{sys.argv[1]: Wprowadz wartosc z zakresu <0-255>}")
	if int(sys.argv[2])<0 or int(sys.argv[2])>7:
		error = 1
		print("{sys.argv[2]: Wprowadz wartosc z zakresu <0-255>}")
	if int(sys.argv[3])<0 or int(sys.argv[3])>255:
		error = 1
		print("{sys.argv[3]: Wprowadz wartosc z zakresu <0-255>}")
	if int(sys.argv[4])<0 or int(sys.argv[4])>255:
		error = 1
		print("{sys.argv[4]: Wprowadz wartosc z zakresu <0-255>}")
	if int(sys.argv[5])<0 or int(sys.argv[5])>255:
		error = 1
		print("{sys.argv[5]: Wprowadz wartosc z zakresu <0-255>}")	
	if not error:
		sense.set_pixel(int(sys.argv[1]), int(sys.argv[2]),
		(int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5])))
		print ("{Sukces}")
	
if len(sys.argv) == 1:
	sense.clear(0,0,0)
	print ("{Sukces}")